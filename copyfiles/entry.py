#!/usr/bin/env python3
# -*- coding: utf-8 -*
import multiprocessing
import datetime, os, re, time, urllib.request, urllib.parse, shutil
import http.cookiejar, configparser

svr_addr = "115.156.135.252"


def getProjectDir():
    curren_path = os.path.realpath(__file__)
    curren_path = os.path.split(curren_path)[0]
    global logs_dir
    logs_dir = os.path.join(curren_path, "logs")
    return curren_path


def parse_conf():
    config = configparser.ConfigParser()
    config.read(os.path.join(getProjectDir(), "conf.ini"))
    if config.has_section("default"):
        global username, password, servername, interval
        default_conf = config["default"]
        username = default_conf["username"]
        password = default_conf["password"]
        interval = int(default_conf["interval"])
        servername = default_conf["server_list"].split(",")
    else:
        write2file(os.path.join(logs_dir, "errors.log"), "Errors in config file.")


def write2file(filename, str):
    if not os.path.exists(logs_dir):
        os.mkdir(logs_dir)
    fp = open(filename, 'a+')
    fp.writelines(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
                  + " " + str + "\n")
    fp.close()


def ping_test(url):
    req1 = urllib.request.Request(url)
    try:
        urllib.request.urlopen(req1)
        return True
    except urllib.request.URLError as e:
        write2file(os.path.join(logs_dir, "errors.log"), e.reason)
        return False


def worker():
    if os.path.exists(logs_dir):
        shutil.rmtree(os.path.join(logs_dir))
    while True:
        if ping_test(root_url):
            opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(http.cookiejar.CookieJar()))
            postdata = urllib.parse.urlencode(postDict).encode("utf-8")
            opener.open(root_url + 'userlogin.php', postdata)
            result = opener.open(root_url + 'index.php', postdata)
            res = result.read().decode("utf-8")
            res = re.findall('<table>(.+?)</table>', res)
            res = re.findall('<td>(.+?)</td>', res[0])
            for vms in res:
                name = re.findall(r'blank">(.+?)</a>', vms)[0]
                for svr in servername:
                    svr = svr.strip()
                    if svr == name:
                        if re.findall(r'color="(.+?)">', vms)[0] == 'red':
                            write2file(os.path.join(logs_dir, "runtime.log"), svr
                                       + " has been occupied!")
                        else:
                            postDict2 = {
                                'sel_num': '12',
                                'ins_id': svr
                            }
                            postdata = urllib.parse.urlencode(postDict2).encode("utf-8")
                            ret = opener.open(apply_url + svr, postdata)
                            if ret is not None:
                                write2file(os.path.join(logs_dir, "runtime.log"),
                                           "Apply for " + svr + " is submitted!")
        else:
            write2file(os.path.join(logs_dir, "errors.log"),
                       "The web server is inaccess1ble!")
        time.sleep(interval)


if __name__ == "__main__":
    parse_conf()

    root_url = "http://" + svr_addr + "/dcms/"
    apply_url = root_url + "/applyins.php?ins_id="
    postDict = {
        'username': username,
        'password': password,
        'submit': 'login'
    }

    p = multiprocessing.Process(target=worker)
    p.start()
