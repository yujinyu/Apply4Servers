FROM ubuntu:18.04

MAINTAINER yujinyu

RUN apt-get update && apt-get install -y python3 && apt-get clean all && mkdir /apply4server
COPY copyfiles/ /apply4server
RUN chmod +x /apply4server/entry.py
CMD "/apply4server/entry.py"


