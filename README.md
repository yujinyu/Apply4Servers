## 使用指南：
##### 本脚本通过使用docker容器的自动重启机制实现了7*24自动申请服务器。运行脚本请先[安装docker-ce](https://docs.docker.com/install/linux/docker-ce/centos/)。

### 1. 下载Apply4Servers源码

`git clone https://gitlab.com/yujinyu/Apply4Servers.git`

### 2. 在配置文件copyfiles/conf.ini中修改用户名（必要）、密码（必要）、申请服务器编号（必要）以及脚本运行周期（单位秒，可选）

### 3. 安装docker，并使用docker build制作docker镜像

`docker build 源码文件夹路径 -t apply:server`

### 4. 使用docker镜像运行申请服务的容器

`docker run --detach --name apply4servers --restart always apply:server`

### 5. 查看运行日志

`docker exec apply4servers ls  /apply4server/logs/`

`docker exec apply4servers cat /apply4server/logs/runtime.log`

`docker exec apply4servers cat /apply4server/logs/errors.log`

